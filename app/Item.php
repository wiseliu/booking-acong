<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Item extends Model {

    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'status', 'stock', 'type', 'image_link',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 7,
            'type' => 3,
        ],
    ];

    /**
     * primary key of table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * relationship functions.
     *
     * @param null
     * @return App\Invoice
     */
    public function invoice() {
        return $this->belongsToMany('App\Invoice')->withTimestamps();
    }

}
