<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Invoice extends Model {

    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'booking_time', 'cancel_time', 'payment_time', 'user_id', 'customer_id','status',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'number' => 5,
            'user_id' => 1,
            'customer_id' => 4,
        ],
    ];

    /**
     * primary key of table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * inverse relationship functions.
     *
     * @param null
     * @return App\Customer
     */
    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    /**
     * relationship functions.
     *
     * @param null
     * @return App\Item
     */
    public function item() {
        return $this->belongsToMany('App\Item')->withTimestamps();
    }

}
