<?php

namespace App\Listeners;

use App\Events\CustomerCreateBookingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Services\MailService;
use App\Listeners\CustomerCreateBookingEvent;

class CustomerCreateBookingListener {

    /**
     * @var MailService
     */
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MailService $mailer) {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CustomerBookingEvent  $event
     * @return void
     */
    public function handle(CustomerCreateBookingEvent $event) {
        $this->mailer->sendMailWithSubjectToCustomer('booking', $event->customer->customer_email);
    }

}
