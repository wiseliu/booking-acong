<?php

namespace App\Listeners;

use App\Events\CustomerPayBookingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerPayBookingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerPayBookingEvent  $event
     * @return void
     */
    public function handle(CustomerPayBookingEvent $event)
    {
        //
    }
}
