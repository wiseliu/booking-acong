<?php

namespace App\Listeners;

use App\Events\CustomerMoveBookingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerMoveBookingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerMoveBookingEvent  $event
     * @return void
     */
    public function handle(CustomerMoveBookingEvent $event)
    {
        //
    }
}
