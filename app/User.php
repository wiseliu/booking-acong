<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;


class User extends Authenticatable {

    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 5,
            'email' => 5,
        ],
    ];

    /**
     * primary key of table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * relationship functions.
     *
     * @param null
     * @return App\Customer
     */
    public function customer() {
        return $this->hasMany('App\Customer');
    }

}
