<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Customer extends Model {

    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone', 'email', 'user_id',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'first_name' => 6,
            'lsst_name' => 5,
            'phone' => 3,
            'email' => 6,
        ],
    ];

    /**
     * primary key of table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * relationship functions.
     *
     * @param null
     * @return App\Invoice
     */
    public function invoice() {
        return $this->hasMany('App\Invoice');
    }

    /**
     * inverse relationship functions.
     *
     * @param null
     * @return App\User
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

}
