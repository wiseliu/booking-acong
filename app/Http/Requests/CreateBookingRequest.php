<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBookingRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'booth_unit.*' => 'required|exists:items,name,type,0',
            'customer_first_name' => 'required|string',
            'customer_last_name' => 'required|string',
            'customer_phone' => 'required|digits_between:6,14',
            'customer_email' => 'required|email|unique:customers,email',
        ];
    }

}
