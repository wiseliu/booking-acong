<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:items,name|string',
            'price' => 'required|numeric|min:1',
            'stock' => 'integer|min:1',
            'status' => 'integer|min:0',
            'type' => 'integer|min:0',
            'image_zip' => 'mimes:zip',
        ];
    }
}
