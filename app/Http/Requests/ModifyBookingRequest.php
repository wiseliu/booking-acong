<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ModifyBookingRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'unit_name' => 'required_without:unit_id',
            'unit_id' => 'required_withou:unit_name',
        ];
    }

}
