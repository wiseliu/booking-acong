<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('home', 'HomeController@index');
    Route::get('test-rel', 'HomeController@testRel');
    Route::get('booking-booth', 'BookingController@index');
    Route::get('get-all-booth', 'BookingController@getAllBoothWithCustomerData');
    Route::get('show-upload', 'HomeController@showUploadForm');
    Route::get('dashboard', 'DashboardController@index');
    Route::get('user-management', 'DashboardController@showUserManagement');
    Route::get('customer-management', 'DashboardController@showCustomerManagement');
    Route::get('get-booth-data/{name}', 'BookingController@getBoothData');
    Route::get('show-invoice/{id}', 'DashboardController@showInvoiceByInvoiceId');
    Route::get('print-invoice/{id}', 'DashboardController@makePDFInvoiceById');
    Route::get('customer-management/', 'DashboardController@showCustomerManagement');
    Route::post('upload-test', 'HomeController@processUpload');
    Route::post('book-booth', 'BookingController@bookBooth');
});
