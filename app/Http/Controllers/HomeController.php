<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Invoice;
use App\Item;
use App\Customer;
use Carbon\Carbon;
use App\Http\Requests\CreateItemRequest;
use File;
use Zipper;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function testRel() {
        dd(public_path() . DIRECTORY_SEPARATOR . 'home-image' . DIRECTORY_SEPARATOR);
    }

    public function showUploadForm() {
        return view('upload');
    }

    public function processUpload(CreateItemRequest $request) {
        $item = Item::create($request->all());
        $file = $request->file('image_zip');
        $filename = $file->getClientOriginalName();
        $filepath = public_path() . DIRECTORY_SEPARATOR . 'home-image' . DIRECTORY_SEPARATOR . $asd->id . $filename;
        File::makeDirectory($filepath);
        $file->move($filepath, $filename);
        Zipper::zip($filepath . DIRECTORY_SEPARATOR . $filename)->extractTo($filepath);
        $item->update(['image_link' => $filepath]);
        return $item;
    }

}
