<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateBookingRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\BookingService;
use App\Http\Controllers\Services\ItemService;
use App\Events\CustomerCreateBookingEvent;
use App\Events\CustomerCancelBookingEvent;
use App\Events\CustomerPaidEvent;
use DB;
use Auth;
use Event;

class BookingController extends Controller {

    /**
     * @var BoothService
     */
    private $item;

    /**
     * @var BookingService
     */
    protected $service;

    /**
     * @var User
     */
    protected $user;

    public function __construct(BookingService $service, ItemService $item) {
        //$this->middleware('auth');
        $this->service = $service;
        $this->user = Auth::user();
        $this->item = $item;
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index() {
        return view('bookingFrontend');
    }

    public function bookBooth(CreateBookingRequest $request) {
        //dd($request->all());
        try {
            $order_with_data = DB::transaction(function() use ($request) {
                        $customer = $this->service->createBookingCustomer($request);
                        $order = $this->service->createOrderByOrderRequestAndCustomer($request, $customer);
                        $order_extras[] = $this->service->createInvoiceItemsByItemName($request, $order);

                        $order_data['customer'] = $customer;
                        $order_data['order'] = $order;
                        $order_data['items'] = $order_extras;
                        return $order_data;
                    });
        } catch (Exception $ex) {
            return $ex->getTraceAsString();
        }
        Event::fire(new CustomerCreateBookingEvent($order_with_data['customer']));
        return redirect()->back()->with('order_data', $order_with_data);
    }

    public function cancelBookingUnitByName(ChangeBookingStatusRequest $request) {
        try {
            $cancelled_booth_and_order = DB::transaction(function() use ($request) {
                        $unit = $this->service->changeItemStatusBy('name', $request->unit_name, 0);
                        $order = $this->service->changeOrderStatusBy('name', $unit, 0);
                        $customer = $order->customer()->get();

                        $data['unit'] = $unit;
                        $data['order'] = $order;
                        $data['customer'] = $customer;
                        return $data;
                    });
        } catch (Exception $ex) {
            return $ex->getTraceAsString();
            //return redirect()->back()->with('fail', $ex);
        }
        Event::fire(new CustomerCancelBookingEvent($cancelled_booth_and_order['customer']));
        return $cancelled_booth_and_order;
        //return redirect()->back()->with('order_data', $cancelled_booth_and_order);
    }

    public function cancelBookingUnitById(ChangeBookingStatusRequest $request) {
        try {
            $cancelled_booth_and_order = DB::transaction(function() use ($request) {
                        $unit = $this->service->changeUnitStatusBy('id', $request->unit_id, 0);
                        $order = $this->service->changeOrderStatusBy('id', $unit, 0);
                        $customer = $order->customer()->get();

                        $data['unit'] = $unit;
                        $data['order'] = $order;
                        $data['customer'] = $customer;
                        return $data;
                    });
        } catch (Exception $ex) {
            return $ex->getTraceAsString();
            //return redirect()->back()->with('fail', $ex);
        }
        Event::fire(new CustomerCancelBookingEvent($cancelled_booth_and_order['customer']));
        return $cancelled_booth_and_order;
        //return redirect()->back()->with('order_data', $cancelled_booth_and_order);
    }

    public function payBookedUnitByName(ChangeBookingStatusRequest $request) {
        try {
            $paid_booth_and_order = DB::transaction(function() use ($request) {
                        $unit = $this->service->changeUnitStatusBy('unit_name', $request->unit_name, 2);
                        $order = $this->service->changeOrderStatusBy('unit_name', $unit, 2);
                        $customer = $order->customer()->get();

                        $data['unit'] = $unit;
                        $data['order'] = $order;
                        $data['customer'] = $customer;
                        return $data;
                    });
        } catch (Exception $ex) {
            return $ex->getTraceAsString();
            //return redirect()->back()->with('fail', $ex);
        }
        Event::fire(new CustomerPaidEvent($paid_booth_and_order['customer']));
        return $paid_booth_and_order;
        //return redirect()->back()->with('order_data', $cancelled_booth_and_order);
    }

    public function payBookUnitById(ChangeBookingStatusRequest $request) {
        try {
            $paid_booth_and_order = DB::transaction(function() use ($request) {
                        $unit = $this->service->changeUnitStatusBy('id', $request->unit_id, 2);
                        $order = $this->service->changeOrderStatusBy('id', $unit, 2);
                        $customer = $order->customer()->get();

                        $data['unit'] = $unit;
                        $data['order'] = $order;
                        $data['customer'] = $customer;
                        return $data;
                    });
        } catch (Exception $ex) {
            return $ex->getTraceAsString();
            //return redirect()->back()->with('fail', $ex);
        }
        Event::fire(new CustomerPaidEvent($paid_booth_and_order['customer']));
        return $paid_booth_and_order;
        //return redirect()->back()->with('order_data', $cancelled_booth_and_order);
    }

    public function getAllBoothWithCustomerData() {
        return response()->json($this->item->getAllBoothWithCustomerData());
    }

    public function getBoothData($name) {
        $data = $this->item->getItemBy('name', $name);
        return response()->json($data);
    }

}
