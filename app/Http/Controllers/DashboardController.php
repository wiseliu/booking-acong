<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Services\ReportService;
use App\Http\Controllers\Services\ItemService;
use Spatie\Permission\Models\Role;
use App\Http\Requests\CreateItemRequest;
use App\Customer;
use App\Invoice;
use App\User;
use App\Item;
use Auth;
use PDF;

class DashboardController extends Controller {

    /**
     * @var ItemService
     */
    private $item;

    /**
     * @var ReportService
     */
    protected $reporting;

    /**
     * @var User
     */
    protected $user;

    public function __construct(ReportService $reporting, ItemService $item) {
        $this->middleware('auth');
        $this->user = Auth::user();
        $this->reporting = $reporting;
        $this->item = $item;
    }

    public function index() {
        $dashboard_data = $this->getDashboardData();
        //dd($dashboard_data);
        return view('dashboard', ['dashboard_data' => $dashboard_data]);
    }

    public function showUserManagement() {
        $users = User::all();
        $roles = Role::all();
        //dd($users);
        return view('user', compact('users', 'roles'));
    }

    public function showCustomerManagement() {
        $customers = Customer::all();
        return view('customer', ['customers' => $customers]);
    }

    public function showInvoiceByInvoiceId($id) {
        $invoice = Invoice::find($id);
        if ($invoice != null) {
            $additions = $invoice->item;
            $sum = $invoice->item->sum('price');
            $invoice_data['invoice'] = $invoice;
            $invoice_data['additions'] = $additions;
            $invoice_data['sum'] = $sum;
            //dd($invoice_data);
            return view('report.invoice', ['invoice_data' => $invoice_data]);
        } else {
            return abort(500);
        }
    }

    public function makePDFInvoiceById($id) {
        $invoice = Invoice::find($id);
        if ($invoice != null) {
            $additions = $invoice->item;
            $sum = $invoice->item->sum('price');
            $invoice_data['invoice'] = $invoice;
            $invoice_data['additions'] = $additions;
            $invoice_data['sum'] = $sum;
            $pdf = PDF::loadView('report.invoicePDF', ['invoice_data' => $invoice_data]);
            return $pdf->inline('invoice.pdf');
        } else {
            return abort(500);
        }
    }

    public function getDashboardData($data_type = 'null') {
        assert($data_type == 'null' || $data_type == 'json');
        $dashboard_data['total_income'] = $this->reporting->getTotalIncome();
        $dashboard_data['available_booth'] = $this->item->getBoothsWithStatus(0, 'count');
        $dashboard_data['booked_booth'] = $this->item->getBoothsWithStatus(1, 'count');
        $dashboard_data['paid_booth'] = $this->item->getBoothsWithStatus(2, 'count');
        if ($this->item->getBookingWithDueDays(14, 'count') != 0) {
            $dashboard_data['due_booth'] = $this->item->getBookingWithDueDays(14, 'count');
            $dashboard_data['due_booth_data'] = $this->item->getBookingWithDueDays(14);
        }
        if ($data_type == 'json') {
            return response()->json($dashboard_data);
        } else {
            return $dashboard_data;
        }
    }

    public function getInvoiceDataByCustomerId($id) {
        $customer = Customer::findOrFail($id);
        $invoices = $this->reporting->getInvoiceByCustomer($customer);
        $invoice_data['customer'] = $customer;
        $invoice_data['invoice_detail'] = $invoices;
        if (!$invoices->isEmpty()) {
            foreach ($invoices as $key => $invoice) {
                $invoice_data['invoice_extras'][$key] = $this->reporting->getOrderExtrasListByCustomerUnit($invoice);
            }
        }
        return response()->json($invoice_data);
    }
    
    public function showCreateItemForm() {
        return view('createItem');
    }
    
    public function createItem(CreateItemRequest $request) {
        Item::create($request->all());
    }

}
