<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Invoice;

class ReportService extends Controller {

    public function getInvoiceByCustomer(Customer $customer) {
        $invoices = Invoice::where('customer_id', $customer->id)->where('status', '>', 0)->get();
        return $invoices;
    }

    public function getInvoiceBy($param = 'id', $value) {
        assert(is_string($param));
        $invoices = Invoice::where($param, $value)->where('booking_status', '>', 1)->get();
        return $invoices;
    }

    public function getOrderItemsListByInvoice(Invoice $invoice) {
        $booth_extras_list = $invoice->item;
        return $booth_extras_list;
    }

    public function getTotalIncome() {
        $sum = 0;
        $all_invoice = Invoice::all();
        foreach ($all_invoice as $invoice)
        {
            $sum += $invoice->item->sum('price');
        }
        return $sum;
    }

}
