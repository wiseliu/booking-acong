<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;

class CustomerService extends Controller {

    public function getAllCustomer() {
        $customers = Customer::all();
        return $customers;
    }

    public function getCustomerById($id) {
        $customer = Customer::find($id);
        return $customer;
    }

    public function getCustomerBy($param = 'customer_name', $value) {
        assert(is_string($param));
        $named_customer = Customer::where($param)->firstOrFail();
        return $named_customer;
    }

    public function createCustomer(CustomerRequest $request) {
        $created_customer = Customer::findOrNew($request->all());
        return $created_customer;
    }

    public function updateCustomer(CustomerRequest $request) {
        $updated_customer = Customer::updateOrCreate($request->all());
        return $updated_customer;
    }

    public function softDeleteCustomerById($id) {
        Customer::find($id)->delete();
    }

    public function softDeleteCustomerBy($param = 'additional_name', $value) {
        assert(is_string($param));
        Customer::where($param, $value)->delete();
    }

}
