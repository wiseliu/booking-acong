<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\ModifyItemRequest;
use Carbon\Carbon;
use App\Customer;
use App\Invoice;
use App\Item;
use Storage;

class ItemService extends Controller {

    public function getAllItem() {
        $booths = Item::all();
        return $booths;
    }

    public function getAllBoothWithCustomerData() {
        $booth_data = array();
        $items_array = Item::where('type', 0)->where('status', '>', 0)->get();
        foreach ($items_array as $booth) {
            $booth_data = array_add($booth_data, $booth->name, $booth->invoice()->first()->customer->first_name);
        }
        return $booth_data;
    }

    public function getBookingWithDueDays($days = 14, $type = '') {
        assert(is_string($type) && is_integer($days));
        $due_booking = [];
        $temp = Invoice::where('status', 1)->where('booking_time', '<=', Carbon::now()->subDays($days))->get();
        foreach ($temp as $order) {
            $due_booking['order'][] = [
                'sum' => $order->item()->sum('price'),
                'customer_id' => $order->customer_id,
                'unit_customer_booking_time' => $order->booking_time,
                'order_data' => $order,
                'order_additional' => $order->item,
            ];
        }
        if ($type == 'count') {
            return $temp->count();
        } else {
            return $due_booking;
        }
    }

    public function getBoothsWithStatus($status = 'free', $type = '') {
        assert(is_string($status) || is_integer($status));
        assert(is_string($type));
        switch ($status) {
            case 'free':
            case 0:
                $status = 0;
                break;
            case 'booked':
            case 1:
                $status = 1;
                break;
            case 'paid':
            case 2:
                $status = 2;
                break;
            default :
                return false;
        }
        $free_booths = Item::where('status', $status)->where('type', 0)->get();
        if ($type == 'count') {
            return $free_booths->count();
        } else {
            return $free_booths;
        }
    }

    public function getItemBy($param = 'name', $value) {
        assert(is_string($param));
        $named_booth = Item::where($param, $value)->get();
        return $named_booth;
    }

    public function createItem(CreateItemRequest $request) {
        $item = Item::create($request->all());
        if ($request->has('image_zip')) {
            $file = $request->file('image_zip');
            $filename = $file->getClientOriginalName();
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'home-image' . DIRECTORY_SEPARATOR . $asd->id . $filename;
            File::makeDirectory($filepath);
            $file->move($filepath, $filename);
            Zipper::zip($filepath . DIRECTORY_SEPARATOR . $filename)->extractTo($filepath);
            $item->update(['image_link' => $filepath]);
        }
        return $item;
    }

    public function updateItem(ModifyItemRequest $request) {
        $updated_item = Item::updateOrCreate($request->all());
        if ($request->has('image_zip')) {
            $file = $request->file('image_zip');
            $filename = $file->getClientOriginalName();
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'home-image' . DIRECTORY_SEPARATOR . $asd->id . $filename;
            File::makeDirectory($filepath);
            $file->move($filepath, $filename);
            Zipper::zip($filepath . DIRECTORY_SEPARATOR . $filename)->extractTo($filepath);
            $updated_item->update(['image_link' => $filepath]);
        }
        return $updated_item;
    }

    public function softDeleteItemBy($param = 'name', $name) {
        assert(is_string($param));
        Item::where($param, $name)->delete();
    }

    public function softDeleteItemById($id) {
        Item::find($id)->delete();
    }

}
