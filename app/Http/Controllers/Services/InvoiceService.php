<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Invoice;
use Carbon\Carbon;

class InvoiceService extends Controller {

    public function getBookingWithDueDays($days = 14, $type = '') {
        assert(is_string($type) && is_integer($days));
        $due_booking = [];
        $temp = Invoice::where('status', 1)->where('booking_time', '<=', Carbon::now()->subDays($days))->get();
        foreach ($temp as $order) {
            $due_booking['order'][] = [
                'sum' => $order->item()->sum('price'),
                'customer_id' => $order->customer_id,
                'unit_customer_booking_time' => $order->booking_time,
                'order_data' => $order,
                'order_additional' => $order->item,
            ];
        }
        if ($type == 'count') {
            return $temp->count();
        } else {
            return $due_booking;
        }
    }

    public function getOrdersWithStatus($status = 'free', $type = '') {
        assert(is_string($status) || is_integer($status));
        assert(is_string($type));
        switch ($status) {
            case 'free':
            case 0:
                $status = 0;
                break;
            case 'booked':
            case 1:
                $status = 1;
                break;
            case 'paid':
            case 2:
                $status = 2;
                break;
            default :
                return false;
        }
        $free_booths = Invoice::where('status', $status)->get();
        if ($type == 'count') {
            return $free_booths->count();
        } else {
            return $free_booths;
        }
    }

}
