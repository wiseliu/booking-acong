<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use Carbon\Carbon;
use App\Customer;
use App\CustomerUnit;

class MailService extends Controller {

    protected $mail_data = [
        'booking-thankyou' => [
            'view' => 'emails.thank-you-booking',
            'subject' => 'Thank You For Booking Our Booth',
        ],
        'change-booking' => [
            'view' => 'emails.change-booking',
            'subject' => 'You Have Changed Your Booth Order',
        ],
        'cancel-booking' => [
            'view' => 'emails.cancel-booking',
            'subject' => 'You Cancelled Your Booking',
        ],
        'due-order' => [
            'view' => 'emails.due-order',
            'subject' => 'Your Order is Due',
        ],
        'default' => [
            'view' => 'emails.default',
            'subject' => 'Please Dont Reply This Email',
        ],
    ];

    public function sendMailWithSubjectToCustomer($subject = 'default', $customer_email) {
        switch ($subject) {
            case 'thank-you':
            case 'booking':
            case 'booking-thank-you':
            case 'booking-thank-you':
                $data = $this->mail_data['booking-thankyou'];
                break;
            case 'change':
            case 'change-booking':
            case 'change booking':
                $data = $this->mail_data['change-booking'];
            case 'cancel':
            case 'cancel-booking':
            case 'cancel booking':
                $data = $this->mail_data['cancel-booking'];
            case 'due':
            case 'due-order':
            case 'due order':
                $data = $this->mail_data['due-order'];
            default:
                $data = $this->mail_data['default'];
        }
        $data['customer_name'] = Customer::where('customer_email', $customer_email)->value('customer_name');
        $data['customer_email'] = $customer_email;
        Mail::send($data['view'], $data, function ($message) use ($data) {
            $message->from('test.laravel.forsecnet@gmail.com');
            $message->to($data['customer_email'], $data['customer_name'])->subject($data['subject'])->cc('admin@nusantaraexpo.com');
            $message->replyTo('test.laravel.forsecnet@gmail.com');
        });
    }

    public function sendMailToCustomerWithDueOrder($days = 14) {
        $due_order_customer = CustomerUnit::where('unit_customer_booking_time', '<=', Carbon::now()->subDays($days))->where('booking_status', 1)->get();
        foreach ($due_order_customer as $order) {
            $this->sendMailWithSubjectToCustomer('due', $order->customer()->value('email'));
        }
    }

}
