<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBookingRequest;
use App\Item;
use App\Invoice;
use App\Customer;
use Carbon\Carbon;
use Auth;

class BookingService extends Controller {

    public function createBookingCustomer(CreateBookingRequest $request) {
        $booking_customer = Customer::create([
                    'first_name' => $request->customer_first_name,
                    'last_name' => $request->customer_last_name,
                    'phone' => $request->customer_phone,
                    'email' => $request->customer_email,
                    'address' => $request->customer_address,
                    'user_id' => 1,
        ]);
        return $booking_customer;
    }

    public function createOrderByOrderRequestAndCustomer(CreateBookingRequest $request, $customer) {
        $timestamp = Carbon::now();
        $user = Auth::user();
        $order = Invoice::create([
                    'number' => 'INV-' . $customer->id . '-' . $timestamp->month . $timestamp->year,
                    'booking_time' => $timestamp,
                    'user_id' => 1,
                    'customer_id' => $customer->id,
                    'status' => 1,
        ]);
        return $order;
    }

    public function createInvoiceItemsByItemName(CreateBookingRequest $request, Invoice $order) {
        $order_extras = array();
        foreach ($request->booth_unit as $booth) {
            $booth_data = Item::where('name', $booth)->where('type', 0)->where('status', 0)->where('stock','>',0)->firstOrFail();
            $booth_data->update([
                'status' => 1,
                'stock' => $booth_data->stock - 1,
            ]);
            $order->item()->attach($booth_data->id);
            $order_extras['units'][] = $booth_data;
        }

        if ($request->has('addon_name')) {
            foreach ($request->addon_name as $additional) {
                $additional_data = Item::where('name', $additional)->where('type', 1)->where('stock', '>', 0)->firstorFail();
                $additional_data->update([
                    'stock' => $additional_data->stock - 1,
                ]);
                $order->item()->attach($additional_data->id);
                $order_extras['items'][] = $additional_data;
            }
        }
        return $order_extras;
    }

    public function changeItemStatusBy($param, $value, $status = 0) {
        assert(is_string($param));
        assert($status <= 2);
        $changed_booth = Item::where($param, $value)->firstOrFail();
        if ($changed_booth->stock >= 0) {
            if ($status == 0) {
                $changed_booth->update([
                    'status' => $status,
                    'stock' => $changed_booth->stock + 1,
                ])->firstOrFail();
            } else {
                $changed_booth->update([
                    'status' => $status,
                    'stock' => $changed_booth->stock - 1,
                ])->firstOrFail();
            }
        }
        return $changed_booth;
    }

    public function changeOrderStatusBy($param, $value, $status = 0) {
        assert(is_string($param));
        assert($status <= 2);
        $timestamp = Carbon::now();
        if ($status == 0) {
            $changed_order = Invoice::where($param, $value)->get()->update([
                        'status' => $status,
                        'cancel_time' => $timestamp,
                    ])->delete();
            $changed_order->items()->detach();
        } elseif ($status == 1) {
            $changed_order = Invoice::where($param, $value)->get()->update([
                'status' => $status,
                'booking_time' => $timestamp,
            ]);
        } else {
            $changed_order = Invoice::where($param, $value)->get()->update([
                'status' => $status,
                'payment_time' => $timestamp,
            ]);
        }
        return $changed_order;
    }

    public function changeCustomerUnitLocation(Invoice $order, $to_unit_name) {
        assert(is_string($to_unit_name));
        $unit_list = $order->items()->where('type', 0)->get();
        $synced_array = $unit_list->pluck('id');
        $updated_order = $order->item()->sync($synced_array)->get();
        return $updated_order;
    }

    public function showBookingWithDueDays($days) {
        $due_booking = Invoice::where('booking_time', '<=', Carbon::now()->subDays($days))->where('status', 1)->get();
        return $due_booking;
    }

}
