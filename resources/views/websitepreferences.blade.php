@extends('layouts.dashboard')

@section('content')

<div class="content ">
    <div class="page-title">
        <h3>Website Preferences </h3>
    </div>
    <div id="container">
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                    </div>
                    <div class="grid-body no-border"> <br>
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <div class="form-group">
                                    <label class="form-label">Website Title</label>
                                    <span class="help">e.g. "cms.com"</span>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="website-name" id="website-name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Website Tagline</label>
                                    <span class="help">e.g. "what is your website about"</span>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="website-tagline" id="website-tagline">
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label class="form-label">Meta  Description</label>
                                    <span class="help">e.g. "what is your website about"</span>
                                    <div class="controls">
                                        <input type="text" class="form-control" name="meta-description" id="meta-description">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-label">Meta Keywords</label>
                                    <div class="controls">
                                        <textarea class="form-control" id="google-analytics" name="meta-keyword"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-label">Google Analytics Code</label>
                                    <div class="controls">
                                        <textarea class="form-control" id="google-analytics" name="google-analytics"></textarea>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE -->
</div>



@endsection
