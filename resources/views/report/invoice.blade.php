{!! HTML::style('plugins/boostrapv3/css/bootstrap.min.css') !!}
{!! HTML::style('css/invoice.css') !!}

<div class="content ">
    <div><img src="{{ url('img/Agralandlogo.png') }}" width="10%" height="10%"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="information-header">Your Invoice</div>
            <div class="invoice-subheader">Unit</div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Unit</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                @foreach($invoice_data['additions'] as $unit)
                <tbody class="purchased-booth-final"><tr>
                        <td class="booth-image">  <img src="http://local.cms.com/img/dummy-booth.jpg"></td>
                        <td class="booth-unit">{{$unit->name}}</td>
                        <td class="booth-price-detail" price={{$unit->price}}>{{$unit->price}}</td>
                    </tr>
                </tbody>
                @endforeach
                <tfoot>
                    <tr>
                        <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                        <td class="total-harga-booth" booth_price="40000000">{{$invoice_data['sum']}}</td>
                    </tr>
                </tfoot>
            </table>

            <!--            <div class="invoice-subheader">Tambahan</div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Component</th>
                                    <th>Qty</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody class="purchased-addon-final"></tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                    <td class="total-harga-addon" addon-price="0">0</td>
                                </tr>
                            </tfoot>
                        </table>-->
            <div class="invoice-subheader">Grand Total</div>
            <div class="grand-total">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: right" colspan="3">Grand Total</th>
                            <th class="grand-total-price" grand-total={{$invoice_data['sum']}}>{{$invoice_data['sum']}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <button type="button" class="btn btn-primary print-btn">Print PDF</button>
        </div>
    </div>
</div>

{!! HTML::script('plugins/jquery-1.11.3.min.js') !!}
{!! HTML::script('js/invoice.js') !!}