<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

        <!-- Styles -->
        {!! HTML::style('plugins/pace/pace-theme-flash.css') !!} 
        {!! HTML::style('plugins/boostrapv3/css/bootstrap.min.css') !!}
        {!! HTML::style('plugins/boostrapv3/css/bootstrap-theme.min.css') !!}
        {!! HTML::style('plugins/font-awesome/css/font-awesome.css') !!}
        {!! HTML::style('css/animate.min.css') !!}

        {!! HTML::style('css/style.css') !!}
        {!! HTML::style('css/responsive.css') !!}
        {!! HTML::style('css/custom-icon-set.css') !!}
    </head>
    <body class="error-body no-top" data-original="{!! URL::to('img/work.jpg') !!}"  style="background-image: url({!! URL::to('img/work.jpg') !!})">
        <div class="container">
            <div class="row login-container animated fadeInUp">  
                <div class="col-md-7 col-md-offset-2 tiles white no-padding">
                    <div class="p-t-30 p-l-40 p-b-20 xs-p-t-10 xs-p-l-10 xs-p-b-10"> 
                        <h2 class="normal">Sign in to webarch</h2>
                        <p>Use Facebook, Twitter or your email to sign in.<br></p>
                        <p class="p-b-20">Sign up Now! for webarch accounts, it's free and always will be..</p>
                        <button type="button" class="btn btn-primary btn-cons" id="login_toggle">Login</button> or&nbsp;&nbsp;<button type="button" class="btn btn-info btn-cons" id="register_toggle"> Create an account</button>
                    </div>
                    <div class="tiles grey p-t-20 p-b-20 text-black">
                        <form id="frm_login" class="animated fadeIn" role="form" method="POST" action="{{ url('/login-request') }}">
                            {!! csrf_field() !!}
                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="email" class="form-control" name="user_email"  placeholder="Email">

                                    @if ($errors->has('user_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" placeholder="Password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="control-group  col-md-10">
                                    <div class="checkbox checkbox check-success"> <a href="#">Trouble login in?</a>&nbsp;&nbsp;
                                        <input type="checkbox" id="checkbox1" value="1">
                                        <label for="checkbox1">Keep me reminded </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <input type="hidden" class="form-control" name="role_id"  value="1">
                                <div class="pull-right" style="margin-right: 18px;">
                                    <button type="submit" class="btn btn-primary">
                                       Login
                                    </button>
                                </div>
                            </div>
                        </form>
                        <form role="form" method="POST" action="{{ url('/register') }}" id="frm_register" class="animated fadeIn" style="display:none">
                            {!! csrf_field() !!}
                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="user_fname"  placeholder="First Name">
                                    <label id="user_fname-Error" class="error" for="form1Amount"></label>
                                    @if ($errors->has('user_fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_fname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="user_lname" placeholder="Last Name">
                                </div>
                            </div>	
                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="col-md-12">
                                    <input name="user_email" id="user_email" type="text"  class="form-control" placeholder="Email Address">
                                    <label id="user_email-Error" class="error" for="form1Amount"></label>
                                     @if ($errors->has('user_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>	

                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password"  placeholder="Password">
                                     <label id="password-Error" class="error" for="form1Amount"></label>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  placeholder="Confirm Password">
                                </div>
                            </div>

                            <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                <div class="pull-right" style="margin-right: 18px;">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>   
            </div>
        </div>



        <!-- JavaScripts -->

        {!! HTML::script('plugins/jquery-1.11.3.min.js') !!}
        {!! HTML::script('plugins/bootstrap/js/bootstrap.min.js') !!}
        {!! HTML::script('plugins/pace/pace.min.js') !!}
        {!! HTML::script('plugins/jquery-validation/js/jquery.validate.min.js') !!}
        {!! HTML::script('plugins/jquery-lazyload/jquery.lazyload.min.js') !!}
        {!! HTML::script('js/login_v2.js') !!}
    </body>
</html>
