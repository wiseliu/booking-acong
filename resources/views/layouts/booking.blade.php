<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        {!! HTML::style('booking-template/css/normalize.css') !!}
        {!! HTML::style('plugins/boostrapv3/css/bootstrap.min.css') !!}
        {!! HTML::style('plugins/boostrapv3/css/bootstrap-theme.min.css') !!}
        {!! HTML::style('plugins/font-awesome/css/font-awesome.css') !!}
        {!! HTML::style('booking-template/css/jquery.fullPage.css') !!}
        {!! HTML::style('booking-template/css/vegas.min.css') !!}
        {!! HTML::style('booking-template/plugins/slick/slick.css') !!}
        {!! HTML::style('booking-template/plugins/slick/slick-theme.css') !!}
        {!! HTML::style('booking-template/css/main.css') !!}
        {!! HTML::script('visions-template/js/modernizr-2.7.1.min.js') !!}
    </head>
    <body>
        <!--BEGIN OF Static video bg  - uncomment below to use Video as Background-->
        <div id="container" class="video-container show-for-medium-up">
            <video autoplay="autoplay" loop="loop" autobuffer="autobuffer" muted="muted" width="640" height="360" style="height: 810px; width: 1440px; margin-top: -27.5px; margin-left: 0px;">
                <source src="{{ url('visions-template/vid/flower_loop.mp4') }}" type="video/mp4">
            </video>
        </div>
        <div class="row" id="booking-form-detail">
            <div class="col-md-12">
                <div class="zone-header">
                    Site Plan
                </div>
                <div class="book-total">
                    Total Booth Booked: <span id="totalBooked">0</span>
                </div>
                <table class="table scroll table-front">
                    <thead>
                        <tr>
                            <th >Gambar</th>
                            <th style="width: 30%">Unit</th>
                            <th style="width: 30%">Harga</th>
                            <th style="width: 30%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="booked-booth">

                    </tbody>
                </table>
                <div class="pull-right">
                    <div class="btn btn-primary" id="booking-btn" data-toggle="modal" data-target="#booking-form">Book</div>
                </div>
            </div>
        </div>

        <div class="row">

            <header>
                <div class="row">
                    <div class="col-md-6">
                        <div class="logo"></div>
                    </div>
                    <div class="col-md-6">
                        <nav id="menu">
                            <ul class="main-nav">
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>

            <div id="fullpage">
                @yield('content')
            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <form method="POST" action="book-booth" id="nusantara-booking-form">
                {{csrf_field()}}
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Booking Form</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="slider single-item">

                                    <!--slide1-->
                                    <div id="form-1">
                                        <div class="col-md-12">
                                            <div class="addon-component-header"> Booth </div>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Picture</th>
                                                        <th >Booth</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="purchased-item">
                                                    <tr>
                                                        <td class="booth-image"></td>
                                                        <td class="both-component">

                                                        </td>
                                                        <td class="both-price-detail">

                                                        </td>
                                                    </tr>


                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                                        <td class="total-harga-booth">Rp, 20,000,000</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="addon-component-header"> Tambahan </div>
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th >Component</th>
                                                        <th>Qty</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="purchased-item purchased-addon">

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                                        <td class="total-harga-addon" addon-price="0"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="total-cost">

                                            </div>
                                        </div>
<!--                                        <div class="col-md-5">
                                            <div class="information-header">Perlengkapan Tambahan</div>
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                Support Teknis
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <div class="form-group" style="margin-left: 15px;">
                                                                <label class="checkbox">
                                                                    <input type="checkbox" class="addon" id="lighting" value="lighting" price="100000"> Lighting
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" class="addon" id="sound" value="sound system" price="200000"> Sound System
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" class="addon" id="videotron" value="videotron" price="300000"> Videotron
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" class="addon" id="plasma" value="Plasma TV" price="100000"> Plasma TV
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                Konten Support
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-left: 15px;">
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="host" value="Host" price="100000"> Host
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="model" value="Model" price="100000"> Model
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="videotron" value="Talkshow" price="100000"> Talkshow
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="plasma" value="Musik" price="100000"> Musik
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="plasma" value="Ide kreatif multimedia" price="100000"> Ide Kreatif Multimedia
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-left: 15px;">
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="lighting" value="Singer" price="100000"> Singer
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="sound" value="Demo Skill" price="100000"> Demo Skill
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="videotron" value="Dancer" price="100000"> Dancer
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="plasma" value="Outdoor Parade" price="100000"> Outdoor Parade
                                                                        </label>
                                                                        <label class="checkbox">
                                                                            <input type="checkbox" class="addon" id="plasma" value="Ide kreatif acara panggung"> Ide Kreatif Acara Panggung
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title">
                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                Form Program
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->

                                    </div>


                                    <!--slide 2-->
                                    <div id="form-2">
                                        <div class="col-md-6">
                                            <div class="information-header">Your Invoice</div>
                                            <div class="invoice-subheader">Booth</div>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th >Picture</th>
                                                        <th>Booth</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="purchased-booth-final">

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                                        <td class="total-harga-booth">Rp, 20,000,000</td>
                                                    </tr>
                                                </tfoot>
                                            </table>

                                            <div class="invoice-subheader">Tambahan</div>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th >Component</th>
                                                        <th>Qty</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="purchased-addon-final">

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                                        <td class="total-harga-addon" addon-price='0'>0</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <div class="invoice-subheader">Grand Total</div>
                                            <div class="grand-total">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: right" colspan="3">Grand Total</th>
                                                            <th class="grand-total-price"></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nama Depan</label>
                                                <input name="customer_first_name" type="text" class="form-control" id="nama_depan" placeholder="Nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nama Belakang</label>
                                                <input name="customer_last_name" type="text" class="form-control" id="nama_belakang" placeholder="Nama Instansi">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">No Telpon</label>
                                                <input name="customer_phone" type="text" class="form-control" id="telepon" placeholder="Nomor Telpon">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input name="customer_email" type="email" class="form-control" id="email" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Alamat</label>
                                                <textarea name="customer_address" class="form-control" id="address" placeholder="Alamat Lengkap"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <!--slide 3-->
                                    <div id="form-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="information-header">Term & Condition</div>
                                                <div class="term-agreement">

                                                </div>
                                                <div class="form-group" style="margin-left: 15px;">
                                                    <label class="checkbox">
                                                        <input type="checkbox" class="agree" id="host" value="Agree"> Agree
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-danger hide prev">Back</button>
                            <button type="button" class="btn btn-primary next">Next</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Modal -->

        <!-- Modal -->
        <div class="modal fade" id="bookingConfirmation" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirm Booking <span id="boothId"></span> <span id="zoneId" style="display: none;"></span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="threesixty" data-path="{{ url('img/threesixty/gem{index}.jpg')}}" data-count="61"></div>
                            </div>
                            <div class="col-md-12">
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal cancel-book" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary book-btn">Book</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->

        <!-- JavaScripts -->
        {!! HTML::script('plugins/jquery-1.11.3.min.js') !!}
        {!! HTML::script('booking-template/js/jquery.imagemapster.min.js') !!}
        {!! HTML::script('plugins/boostrapv3/js/bootstrap.min.js') !!}
        {!! HTML::script('booking-template/js/jquery.fullPage.min.js') !!}
        {!! HTML::script('booking-template/plugins/slick/slick.min.js') !!}
        {!! HTML::script('booking-template/js/jquery.threesixty.js') !!}
        {!! HTML::script('booking-template/js/sum.js') !!}
        {!! HTML::script('booking-template/js/main.js') !!}


    </body>


</html>
