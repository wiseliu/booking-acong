<div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper">
            <div class="profile-wrapper"> <img src="assets/img/profiles/avatar.jpg"  alt="" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" width="69" height="69" /> </div>
            <div class="user-info">
                <div class="greeting">Welcome</div>
                <div class="username">John <span class="semi-bold">Smith</span></div>
                <div class="status">Status<a href="#">
                        <div class="status-icon green"></div>
                        Online</a></div>
            </div>
        </div>
        <!-- END MINI-PROFILE -->
        <!-- BEGIN SIDEBAR MENU -->
        <ul id="main-navigation">
            <li class=""> <a href="dashboard" id="dashboard"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> </a></li>
<!--            <li class=""> <a href="javascript:;"> <i class="fa fa-file-o"></i> <span class="title">Page Design</span>  </a></li>
            <li class=""> <a href="javascript:;"> <i class="fa fa-pencil-square-o"></i> <span class="title">Blog</span>  </a></li>
            <li class=""> <a href="media" id="media"> <i class="fa fa-image"></i> <span class="title">Media</span>  </a></li>
            <li class=""> <a href="javascript:;"> <i class="fa fa-plug"></i> <span class="title">Plugins</span></a>-->
<!--                <ul class="sub-menu">
                    <li> <a href="booking-system" id="booking-system">Booking System</a></li>
                </ul>
            </li>-->
             <li> <a href="booking-system" id="booking-system"><i class="fa fa-book"></i> <span class="title">Report</span></a></li>
             <li> <a href="user-management" id="user-management"><i class="fa fa-user"></i> <span class="title">User Management</span></a> </li>
             <li> <a href="customer-management" id="customer-management"><i class="fa fa-user"></i> <span class="title">Customer Management</span></a> </li>
<!--            <li class=""> <a href="javascript:;"> <i class="fa fa-gear"></i> <span class="title">Configuration</span> <span class="arrow "></span> </a>
                <ul class="sub-menu">
                    <li> <a href="website-preferences" id="website-preferences">Website Preferences</a></li>
                    <li> <a href="user-management" id="user-management">User Management</a> </li>
                </ul>
            </li>-->

        </ul>

        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<a href="#" class="scrollup">Scroll</a>
