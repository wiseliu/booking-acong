@extends('layouts.dashboard')

@section('content')
<div class="content ">

    <div class="row">

        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles blue ">
                <div class="tiles-body">
                    <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                    <div class="tiles-title"> TOTAL AVAILABLE BOOTH </div>
                    <div class="heading"> <span class="animate-number" data-value={{ $dashboard_data['available_booth'] }} data-animation-duration="1200">100</span> Booth </div>
                    <div class="progress transparent progress-small no-radius">
                        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%" style="width: 26.8%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles red ">
                <div class="tiles-body">
                    <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                    <div class="tiles-title"> TOTAL BOOKED BOOTH </div>
                    <div class="heading"> <span class="animate-number" data-value={{ $dashboard_data['booked_booth'] }} data-animation-duration="1200">100</span> Booth </div>
                    <div class="progress transparent progress-small no-radius">
                        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%" style="width: 26.8%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles green ">
                <div class="tiles-body">
                    <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                    <div class="tiles-title"> TOTAL PAID BOOTH </div>
                    <div class="heading"> <span class="animate-number" data-value={{ $dashboard_data['paid_booth'] }} data-animation-duration="1200">100</span> Booth </div>
                    <div class="progress transparent progress-small no-radius">
                        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%" style="width: 26.8%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles gray ">
                <div class="tiles-body">
                    <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                    <div class="tiles-title"> TOTAL INCOME </div>
                    <div class="heading"> <span class="animate-number" data-value={{'Rp.' .$dashboard_data['total_income'] }} data-animation-duration="1200">Rp. 20000000</span> </div>
                    <div class="progress transparent progress-small no-radius">
                        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%" style="width: 26.8%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-vlg-12 m-b-12 ">
            <div class="tiles white">
                <div class="row"></div>
                <h5 class="semi-bold m-t-30 m-l-30">UNPAID LIST</h5>
                <div class="pull-right m-r-30 m-b-15 clearfix">
                    <div class="btn-group m-b-10" data-toggle="buttons-checkbox">
                        <button type="button" class="btn btn-small btn-primary active">Ascending Due Time</button>
                        <button type="button" class="btn btn-small btn-primary" aria-pressed="false">Descending Due Time</button>
                    </div>
                </div>
                <table class="table no-more-tables m-t-20 m-l-20 m-b-30">
                    <thead>
                        <tr>
                            <th>Consumer</th>
                            <th>Due Time</th>
                            <th>Due Payment</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    @if(isset($dashboard_data['due_booth_data']))
                    @foreach($dashboard_data['due_booth_data']['order'] as $due_booth)
                    <tbody>
                        <tr>
                            <td class="v-align-middle bold text-success">{{ $due_booth['customer_id'] }}</td>
                            <td class="v-align-middle">{{ $due_booth['unit_customer_booking_time'] }}</td>
                            <td class="v-align-middle">{{ $due_booth['sum'] }}</td>
                            <td class="v-align-middle"><span class="label label-important">Unpaid</span></td>
                            <td class="v-align-middle"><button type="button" data-toggle="modal" data-target="#viewConsumer" class="btn btn-small btn-primary">View</button</td>
                        </tr>
                    </tbody>
                    @endforeach
                    @else
                    <tbody>
                        <tr>
                            <td colspan="5" class="v-align-middle">Good, there are no unpaid booth</td>
                        </tr>
                    </tbody>
                    @endif
                </table>
                <div class="row"></div>
            </div>
        </div>

    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="viewConsumer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Your Invoice</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="information-header">Your Invoice</div>
                        <div class="invoice-subheader">Booth</div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Picture</th>
                                    <th>Booth</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody class="purchased-booth-final"><tr>
                                    <td class="booth-image">  <img src="http://local.cms.com/img/dummy-booth.jpg"></td>
                                    <td class="booth-unit">4a</td>
                                    <td class="booth-price-detail" price="20000000">Rp. 20000000</td>
                                </tr>
                                <tr>
                                    <td class="booth-image">  <img src="http://local.cms.com/img/dummy-booth.jpg"></td>
                                    <td class="booth-unit">5a</td>
                                    <td class="booth-price-detail" price="20000000">Rp. 20000000</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                    <td class="total-harga-booth" booth_price="40000000">Rp. 40,000,000</td>
                                </tr>
                            </tfoot>
                        </table>

                        <div class="invoice-subheader">Tambahan</div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Component</th>
                                    <th>Qty</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody class="purchased-addon-final"></tbody>
                            <tfoot>
                                <tr>
                                    <td style="text-align: right; font-weight: bold" colspan="2">Total Harga</td>
                                    <td class="total-harga-addon" addon-price="0">0</td>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="invoice-subheader">Grand Total</div>
                        <div class="grand-total">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="text-align: right" colspan="3">Grand Total</th>
                                        <th class="grand-total-price" grand-total="40000000">Rp. 40,000,000</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger">Authorize Payment</button>
            </div>
        </div>
    </div>
</div>
@endsection
