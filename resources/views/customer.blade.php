@extends('layouts.dashboard')

@section('content')

<div class="content ">
    <div class="page-title">
        <h3>Customer Management </h3>
    </div>
    <div id="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple ">
                            <div class="grid-body no-border">
                                <div class="grid-title no-border">
                                    <h4>Customer <span class="semi-bold">List</span></h4>
                                    <div class="management-tools-wrapper"> 
                                        <a href="javascript:;" class="management-tools" id="delete-selected-items"><i class="fa fa-trash"></i> Delete Selected Customer</a> 
                                        <a class="management-tools" id="add-item" data-toggle="modal" data-target="#add-item-modal"><i class="fa fa-plus-circle"></i> Add New Customer</a> 

                                    </div>
                                </div>
                                @foreach ($errors->all() as $error)
                                <p class="error">{{ $error }}</p>
                                @endforeach
                                <table class="table table-bordered table-hover no-more-tables">
                                    <thead>
                                        <tr>
                                            <th class='text-center'><div class="checkbox check-default ">
                                        <input id="checkbox1" type="checkbox" value="1" class="checkall">
                                        <label for="checkbox1"></label>
                                    </div>
                                    </th>
                                    <th class='text-center'>First Name</th>
                                    <th class='text-center'>Last Name</th>
                                    <th class='text-center'>Email</th>
                                    <th class='text-center' style="width: 30%;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if ( !$customers->count() )
                                        You have no customers
                                        @else

                                    <ul>
                                        @foreach( $customers as $customer )
                                        <tr>
                                            <td class='text-center'>
                                                <div class="checkbox check-default">
                                                    <input id="checkbox2" type="checkbox" value="1">
                                                    <label for="checkbox2"></label>
                                                </div>
                                            </td>
                                            <td class='text-center'>{!! $customer->first_name !!}</td>
                                            <td class='text-center'>{!! $customer->last_name !!}</td>
                                            <td class='text-center'>{!! $customer->email !!}</td>
                                        <input type="hidden" name="user_id" value="{{ $customer->id }}" id="user-id"/>
                                        <td tyle="width: 30%;">
                                            <button id="{{ $customer->id }}"  data-toggle="modal" data-target="#update-item-modal" type="button" class="btn btn-small btn-default btn-cons update-item"><i class="fa fa-pencil-square"></i>&nbsp;Edit</button>
                                            <button id="{{ $customer->id }}"  data-toggle="modal" data-target="#delete-item-modal" type="button" class="btn btn-small btn-danger btn-cons delete-item"><i class="fa fa-trash"></i>&nbsp;Delete</button>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </ul>
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE -->
</div>

<!--Add Modal -->
<div class="modal fade" id="add-item-modal" tabindex="-1" role="dialog" aria-labelledby="add-item-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <br>
                <i class="fa fa-user fa-7x"></i>
                <h4 id="myModalLabel" class="semi-bold">Add New Customer</h4>
                <br>
            </div>
            <form role="form" method="POST" action="{{ url('/store-user') }}" id="register-form">
                {!! csrf_field() !!}

                @if ($errors->any())
                <div class='flash alert-danger'>
                    @foreach ( $errors->all() as $error )

                    <div class="alert alert-error">
                        <button class="close" data-dismiss="alert"></button>
                        <p>{{ $error }}</p>

                    </div>
                    @endforeach
                </div>
                @endif

                <div class="modal-body">
                    <div class="row form-row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="first_name" name="first_name"  placeholder="First Name">
                            <label id="first_name-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('first_name'))
                            <label id="first_name" class="error" for="form1Amount"><strong>{{ $errors->first('first_name') }}</strong></label>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="last_namee" name="last_namee"  placeholder="Last Name">
                            <label id="last_namee-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('last_namee'))
                            <label id="last_namee" class="error" for="form1Amount"><strong>{{ $errors->first('last_namee') }}</strong></label>
                            @endif
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6">
                            <input type="text" name="email"  id="emailAddress"  class="form-control" placeholder="Email Address">
                            <label id="email-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('email'))
                            <label id="emailAddressError" class="error" for="form1Amount"><strong>{{ $errors->first('email') }}</strong></label>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="phone"  id="phone"  class="form-control" placeholder="Phone Number">
                            <label id="phone-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('phone'))
                            <label id="phoneError" class="error" for="form1Amount"><strong>{{ $errors->first('phone') }}</strong></label>
                            @endif
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <textarea class="form-control" rows="3" placeholder="Address" name="address" id="address"></textarea>
                            <label id="address-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('address'))
                            <label id="addressError" class="error" for="form1Amount"><strong>{{ $errors->first('address') }}</strong></label>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="test" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--Update  Modal -->
<div class="modal fade" id="update-item-modal" tabindex="-1" role="dialog" aria-labelledby="update-item-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <br>
                <i class="fa fa-user fa-7x"></i>
                <h4 id="myModalLabel" class="semi-bold">Edit Customer</h4>
                <br>
            </div>
            <form role="form" method="POST" action="{{ url('/update-user/') }}" id="update-form">
                {!! csrf_field() !!}

                @if ($errors->any())
                <div class='flash alert-danger'>
                    @foreach ( $errors->all() as $error )

                    <div class="alert alert-error">
                        <button class="close" data-dismiss="alert"></button>
                        <p>{{ $error }}</p>

                    </div>
                    @endforeach
                </div>
                @endif

                <div class="modal-body">
                    <div class="row form-row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="edit_user_first_name" name="user_first_name"  placeholder="First Name">
                            <label id="user_first_name-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('user_first_name'))
                            <label id="user_first_name" class="error" for="form1Amount"><strong>{{ $errors->first('user_first_name') }}</strong></label>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="edit_user_last_namee" name="user_last_namee"  placeholder="Last Name">
                            <label id="user_first_name-Error" class="error" for="form1Amount"></label>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6">
                            <input type="text" name="user_email"  id="edit_user_email"  class="form-control" placeholder="Email Address">
                            <label id="user_email-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('user_email'))
                            <label id="emailAddressError" class="error" for="form1Amount"><strong>{{ $errors->first('user_email') }}</strong></label>
                            @endif
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6">
                            <input type="password" id="password" class="form-control" name="password"  placeholder="Password">
                            <label id="password-Error" class="error" for="form1Amount"></label>
                            @if ($errors->has('password'))
                            <label id="passwordError" class="error" for="form1Amount"><strong> {{ $errors->first('password') }} </strong></label>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  placeholder="Confirm Password">
                        </div>
                    </div>
                    <input type="hidden" name="user_id" id="edit_user_id"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="test" class="btn btn-primary">Update changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Delete  Modal -->
<div class="modal fade" id="delete-item-modal" tabindex="-1" role="dialog" aria-labelledby="delete-item-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{{ url('/delete-user/') }}" id="delete-form">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <h3>Are You Sure ?</h3>
                    <input type="hidden" id="delete-user-id" name="delete-user-id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" id="test" class="btn btn-danger">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
