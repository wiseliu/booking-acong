@extends('layouts.booking')

@section('content')
<div class="row section active" id="zone-a">
    <div class="col-md-6">
        <div class="map">
<!--            <img src="{{ url('booking-template/img/zone-a.png') }}" border="0" usemap="#map" class="map" id="zones" />
            <map name="map">
                <area shape="rect" coords="201,327,257,382"  alt="booth1" href="#" name="1a" id="1a" class="map-btn" zone="booth-zone-a"  />
                <area shape="rect" coords="257,330,312,382"  alt="booth2" href="#" name="2a" id="2a" class="map-btn" zone="booth-zone-a" />
                <area shape="rect" coords="311,327,365,381"  alt="booth3" href="#" name="3a" id="3a" class="map-btn" zone="booth-zone-a" />
                <area shape="rect" coords="367,329,417,381"  alt="booth4" href="#" name="4a" id="4a" class="map-btn" zone="booth-zone-a" />
                <area shape="rect" coords="420,329,473,381"  alt="booth5" href="#" name="5a" id="5a" class="map-btn" zone="booth-zone-a" />
            </map>-->

            <img src="{{ url('booking-template/img/map-home.png') }}"  border="0" usemap="#map" class="map" id="zones"/>

            <map name="map">
                <area shape="rect" coords="403,367,480,481"  href="#" name="k1" id="k1" class="map-btn"/>
                <area shape="rect" coords="480,367,561,480"  href="#" name="k2" id="k2" class="map-btn"/>
                <area shape="rect" coords="560,367,638,480"  href="#" name="k3" id="k3" class="map-btn"/>
                <area shape="rect" coords="638,365,715,481"  href="#" name="k5" id="k5" class="map-btn"/>
                <area shape="rect" coords="716,365,793,481"  href="#" name="k6" id="k6" class="map-btn"/>
                <area shape="rect" coords="794,364,873,482"  href="#" name="k7" id="k7" class="map-btn"/>
                <area shape="rect" coords="873,364,951,483"  href="#" name="k8" id="k8" class="map-btn"/>
                <area shape="rect" coords="951,364,1029,483"  href="#" name="k9" id="k9" class="map-btn"/>
                <area shape="rect" coords="1029,366,1107,482"  href="#" name="k10" id="k10" class="map-btn"/>
                <area shape="rect" coords="1109,365,1183,481"  href="#" name="k11" id="k11" class="map-btn"/>
                <area shape="rect" coords="1185,364,1262,481"  href="#" name="k12" id="k12" class="map-btn"/>
                <area shape="rect" coords="422,483,636,716"  href="#" name="a1" id="a1" class="map-btn"/>
                <area shape="rect" coords="640,482,791,716"  href="#" name="a2" id="a2" class="map-btn"/>
                <area shape="rect" coords="795,483,950,715"  href="#" name="a3" id="a3" class="map-btn"/>
                <area shape="rect" coords="953,484,1107,717"  href="#" name="a5" id="a5" class="map-btn"/>
                <area shape="poly" coords="1109,485,1303,482,1275,724,1106,717,1108,578"  href="#" name="a6" id="a6" class="map-btn"/>
                <area shape="rect" coords="197,854,314,1089"  href="#" name="b12" id="b12" class="map-btn"/>
                <area shape="rect" coords="314,853,432,1089"  href="#" name="b10" id="b10" class="map-btn"/>
                <area shape="rect" coords="433,854,547,1090"  href="#" name="b8" id="b8" class="map-btn"/>
                <area shape="rect" coords="549,855,668,1090"  href="#" name="b6" id="b6" class="map-btn"/>
                <area shape="rect" coords="668,854,785,1090"  href="#" name="b3" id="b3" class="map-btn"/>
                <area shape="poly" coords="784,855,893,856,921,864,938,887,952,906,949,1090,786,1090,785,1088"  href="#" name="b1" id="b1" class="map-btn"/>
                <area shape="rect" coords="197,1089,313,1324"  href="#" name="b15" id="b15" class="map-btn"/>
                <area shape="rect" coords="315,1089,430,1323"  href="#" name="b11" id="b11" class="map-btn"/>
                <area shape="rect" coords="432,1090,548,1324"  href="#" name="b9" id="b9" class="map-btn"/>
                <area shape="rect" coords="549,1090,667,1324"  href="#" name="b7" id="b7" class="map-btn"/>
                <area shape="rect" coords="668,1091,783,1323"  href="#" name="b5" id="b5" class="map-btn"/>
                <area shape="poly" coords="785,1088,950,1089,951,1273,943,1287,930,1302,919,1314,898,1323,785,1324"  href="#" name="b2" id="b2" class="map-btn"/>
                <area shape="rect" coords="198,1422,313,1657"  href="#" name="c12" id="c12" class="map-btn"/>
                <area shape="rect" coords="314,1422,433,1656"  href="#" name="c10" id="c10" class="map-btn"/>
                <area shape="rect" coords="434,1423,548,1658"  href="#" name="c8" id="c8" class="map-btn"/>
                <area shape="rect" coords="550,1423,665,1657"  href="#" name="c6" id="c6" class="map-btn"/>
                <area shape="rect" coords="667,1422,785,1656"  href="#" name="c3" id="c3" class="map-btn"/>
                <area shape="poly" coords="785,1423,882,1424,920,1430,939,1449,949,1469,950,1658,785,1656"  href="#" name="c1" id="c1" class="map-btn"/>
                <area shape="rect" coords="199,1657,313,1891"  href="#" name="c15" id="c15" class="map-btn"/>
                <area shape="rect" coords="314,1656,431,1892"  href="#" name="c11" id="c11" class="map-btn"/>
                <area shape="rect" coords="432,1657,550,1891"  href="#" name="c9" id="c9" class="map-btn"/>
                <area shape="rect" coords="550,1657,664,1890"  href="#" name="c7" id="c7" class="map-btn"/>
                <area shape="rect" coords="668,1659,783,1891"  href="#" name="c5" id="c5" class="map-btn"/>
                <area shape="poly" coords="785,1656,951,1659,950,1830,941,1867,921,1882,893,1893,784,1892"  href="#" name="c2" id="c2" class="map-btn"/>
            </map>
        </div>
        <div class="row legend-wrapper">
            <div class="col-md-7">
                <div class="information-header">Legend</div>
                <ul class="legend">
                    <li><div class="box yellow"></div> Booth Peserta (3x3 meter) No 1 - 300</li>
                    <li><div class="box blue"></div> Booth Sponsor (3x3 meter) No 45 - 56</li>
                    <li><div class="box blue"></div> Booth Sponsor (2x3 meter) No 1 - 44 & No 57 - 100</li>
                </ul>
            </div>
            <div class="col-md-5">
                <div class="information-header">Keterangan Zona A</div>
                <table class="info-table">
                    <tr>
                        <td>Total Unit</td>
                        <td width="20px">:</td>
                        <td class="unit"><span>200</span> Unit</td>
                    </tr>
                    <tr>
                        <td>Sisa Unit</td>
                        <td width="20px">:</td>
                        <td class="unit"><span>100</span> Unit</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</div>

@endsection
