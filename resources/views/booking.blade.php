@extends('layouts.booking')

@section('content')

<div class="content ">
    <div class="page-title">
        <h3>Booking System Dashboard</h3>
    </div>
    <div id="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple ">
                            <div class="grid-body no-border">
                                <div class="grid-title no-border">
                                    <h4>User <span class="semi-bold">List</span></h4>
                                    <div class="management-tools-wrapper"> 
                                        <a href="javascript:;" class="management-tools" id="delete-selected-items"><i class="fa fa-trash"></i> Delete Selected Users</a> 
                                        <a class="management-tools" id="add-item" data-toggle="modal" data-target="#add-item-modal"><i class="fa fa-plus-circle"></i> Add New User</a> 

                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE -->
</div>



@endsection
