@extends('layouts.dashboard')

@section('content')

<div class="content ">
    <div class="page-title">
        <h3>Media Management </h3>
    </div>
    <div id="container">
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                    <div class="grid-body no-border">
                        <div class="grid-title no-border">
                            <div class="management-tools-wrapper"> 
                                <a href="javascript:;" class="management-tools" id="delete-selected-items"><i class="fa fa-trash"></i> Delete Selected Items</a> 
                                <a class="management-tools" id="add-item" data-toggle="modal" data-target="#add-item-modal"><i class="fa fa-plus-circle"></i> Upload</a> 
                            </div>
                        </div>

                        <div class="gallery-grid">
                            <ul class="gallery-list">
                                <li class="img-thumbnail">
                                    {!! HTML::image('/media-gallery/img1.jpg','title', array('class' => 'thumb')) !!}
                                </li>
                                <li class="img-thumbnail">
                                    {!! HTML::image('/media-gallery/img1.jpg','title', array('class' => 'thumb')) !!}
                                </li>
                            </ul>
                            <div class='clearfix'></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE -->
</div>


<!--Update  Modal -->
<div class="modal fade" id="add-item-modal" tabindex="-1" role="dialog" aria-labelledby="update-item-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="myModalLabel" class="semi-bold">Upload Media</h4>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <form action="/file-upload" class="dropzone no-margin dz-clickable">
                        <div class="dz-default dz-message upload-media"><span>Drop files here to upload</span></div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Upload</button>
            </div>
        </div>
    </div>
</div>

<!--Delete  Modal -->
<div class="modal fade" id="delete-item-modal" tabindex="-1" role="dialog" aria-labelledby="delete-item-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="POST" action="{{ url('/delete-media/') }}" id="delete-form">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <h3>Are You Sure ?</h3>
                    <input type="hidden" id="delete-user-id" name="delete-user-id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" id="test" class="btn btn-danger">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div


@endsection
