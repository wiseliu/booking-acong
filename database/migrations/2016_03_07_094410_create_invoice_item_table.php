<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoice_item', function (Blueprint $table) {
            
            $table->integer('invoice_id')->unsigned();
            $table->foreign('invoice_id')
                    ->references('id')->on('invoices')
                    ->onDelete('cascade');

            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')
                    ->references('id')->on('items')
                    ->onDelete('cascade');

            $table->primary(['invoice_id', 'item_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('invoice_item');
    }

}
