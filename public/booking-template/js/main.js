/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {

    var pathArray = window.location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var baseurl = protocol + '//' + host;

    var addon = '';
    var price = '';
    var bookedBooth = [];
    getAllBoothStatus(baseurl);

    $('.map-btn').css("background-color", "#000000");
    $(document).on('click', '.addon', function() {
        addon = $(this).val();
        price = $(this).attr('price');
        var totalPriceArray = [];

        var table = '<tr id="row-' + addon.split(' ').join('-') + '">\n\
                        <td class="addon-name">' + addon + '<input type="hidden" name="addon_name[]" value="' + addon + '" /></td>\n\
                        <td><input type="text" name="addon_qty[]" class="form-control purchased-qty" id="' + addon + '" placeholder="Qty" value="1" /></td>\n\
                        <td class="addon-price" base-price="' + price + '">' + price + '</td>\n\
                        <input type="hidden" name="addon_price[]" value="' + price + '"/> \n\
                    </tr>\n\
                    ';
        if ($(this).is(':checked')) {
            $('.purchased-addon').append(table).hide().fadeIn();
        } else {
            $('.purchased-addon').find('#row-' + addon.split(' ').join('-')).remove();
        }
        $('.purchased-addon').find('.addon-price').each(function() {
            totalPriceArray.push(parseInt($(this).attr('base-price')));
        });
        var totalPrice = 0;

        for (var i = 0; i < totalPriceArray.length; i++) {
            totalPrice += totalPriceArray[i];
        }

        $('.addon-price').digits();
        $('.total-harga-addon').attr('addon-price', totalPrice).html(totalPrice).digits();
    });

    $(document).on('keyup', '.purchased-qty', function() {
        var currentPrice = parseInt($(this).parent().parent().find('.addon-price').attr('base-price').replace(',', ''));
        var qty = $(this).val();
        var totalPriceArray = [];
        if (qty != 0) {
            $(this).parent().parent().find('.addon-price').text(qty * currentPrice);
        }

        $('.purchased-addon').find('.addon-price').each(function() {
            totalPriceArray.push(parseInt($(this).text().replace(',', '')));
        });
        var totalPrice = 0;

        for (var i = 0; i < totalPriceArray.length; i++) {
            totalPrice += totalPriceArray[i];
        }

        $('.addon-price').digits();
        $('.total-harga-addon').html(totalPrice).digits();
    });

    $(document).on('click', '.next', function() {
        if ($('.prev').hasClass('hide')) {
            $('.prev').removeClass('hide');
            $('.close-modal').hide();
        } else if ($('.slider').find('.slick-active').attr('id') == 'form-3') {
            $('.next').html('Book').addClass('book');
            $('.book').attr('disabled', 'disabled');
        }
        if ($('.slider').find('.slick-active').attr('id') == 'form-2') {
            $('.purchased-booth-final').html

            var boothTable = '';
            $('#purchased-item').find('tr').each(function() {
                var img = $(this).find('.booth-image').html();
                var unit = $(this).find('.booth_unit').text();
                var price = $(this).find('.booth_price-detail').attr('price');
                boothTable += '<tr>\n\
                        <td class="booth-image">' + img + '</td>\n\
                        <td class="booth-unit">' + unit + '</td>\n\
                        <td class="booth-price-detail" price="' + price + '">Rp. ' + price + '</td>\n\
                    </tr>\n\
                    ';

            });
            $('.purchased-booth-final').html(boothTable);
            $('.booth_price-detail').digits();

            var table = '';
            $('.purchased-addon').find('tr').each(function() {
                var addon = $(this).find('.addon-name').text();
                var qty = $(this).find('.purchased-qty').val();
                var price = $(this).find('.addon-price').attr('base-price');
                table += '<tr id="row-' + addon.split(' ').join('-') + '">\n\
                        <td class="addon-name">' + addon + '</td>\n\
                        <td>' + qty + '</td>\n\
                        <td class="addon-price" base-price="' + price + '">Rp. ' + price + '</td>\n\
                    </tr>\n\
                    ';
            });
            $('.purchased-addon-final').html(table);
            $('.addon-price').digits();

            var grandTotal = parseInt($('.total-harga-addon').attr('addon-price')) + parseInt($('.total-harga-booth').attr('booth_price'));
            $('.grand-total-price').attr('grand-total', grandTotal).html("Rp. " + grandTotal);
            $('.grand-total-price').digits();



        }
    });

    $(document).on('click', '.agree', function() {
        if ($(this).is(':checked')) {
            $('.book').removeAttr('disabled');
        } else {
            $('.book').attr('disabled', 'disabled');
        }
    });

    $(document).on('click', '.book', function() {
        $('#nusantara-booking-form').submit();
    });

    $(document).on('click', '.prev', function() {
        if ($('.slider').find('.slick-active').attr('id') == 'form-1') {
            $('.prev').addClass('hide');
            $('.close-modal').show();
        } else if ($('.slider').find('.slick-active').attr('id') == 'form-3') {
            $('.next').html('Book').addClass('.book');
        } else if ($('.slider').find('.slick-active').attr('id') == 'form-2' || $('.slider').find('.slick-active').attr('id') == 'form-1') {
            $('.next').html('Next').removeClass('book');
            $('.next').removeAttr('disabled');
        }
    });

//    $('.map').maphilight({
//        fillColor: 'FF0000',
//        fillOpacity: 1,
//    });
//
//    $('map').imageMapResize();

    $(document).on('click', '.map-btn', function(e) {
        var id = $(this).attr('id');
        var zone = $(this).attr('zone');

        $('#boothId').html(id);
        $('#zoneId').html(zone);

        if($(this).hasClass('booked')){
            $('map').find('#' + id).removeClass('booked');
            var data = $('map').find('#' + id).data('maphilight') || {};
            data.alwaysOn = !data.alwaysOn
            $('map').find('#' + id).data('maphilight', data).trigger('alwaysOn.maphilight');
            $('.booked-booth').find('#booth-' + id).remove();
            $('.map').find('#' + id).mapster('deselect');
        }
        else if($(this).hasClass('booth-booked')){
            e.stopPropagation();
        }
        else {
            $('#bookingConfirmation').modal('show');
            $('#bookingConfirmation').on('shown.bs.modal', function(){
                $('.threesixty').threeSixty({
                    dragDirection: 'horizontal',
                    useKeys: false
                });
            })
        }

        if ($('.booked-booth').find('tr').length > 0) {
            $('#booking-btn').fadeIn();
        } else {
            $('#booking-btn').fadeOut();
        }
        

    });

    $(document).on('click', '.book-btn', function(e){
        $('#bookingConfirmation').modal('hide');

        var boothId = $('#boothId').html();
        var zoneId = $('#zoneId').html();

        $('#'+boothId).toggleClass("booked");
        if ($('#'+boothId).hasClass('booth-booked')) {
            e.stopPropagation();
        } else {
            if ($('#'+boothId).hasClass("map-btn booked")) {
                getBoothInfo(boothId, zoneId, baseurl);
            }
            else {
                removeBoothInfo(boothId, zoneId);
            }
        }

        if ($('.booked-booth').find('tr').length > 0) {
            $('#booking-btn').fadeIn();
        } else {
            $('#booking-btn').fadeOut();
        }
    })

    $('.cancel-book').on('click', function(e){
        var boothId = $('#boothId').html();

        $('map').find('#' + boothId).removeClass('booked');
        var data = $('map').find('#' + boothId).data('maphilight') || {};
        data.alwaysOn = !data.alwaysOn
        $('map').find('#' + boothId).data('maphilight', data).trigger('alwaysOn.maphilight');
        $('.booked-booth').find('#booth-' + boothId).remove();
        $('.map').find('#' + boothId).mapster('deselect');
        
        if ($('.booked-booth').find('tr').length > 0) {
            $('#booking-btn').fadeIn();
        } else {
            $('#booking-btn').fadeOut();
        }
    })



    $(document).on('click', '.batal', function() {

        var id = $(this).parent().parent().attr('id');
        $('map').find('#' + id.substring(6)).removeClass('booked');
        var data = $('map').find('#' + id.substring(6)).data('maphilight') || {};
        data.alwaysOn = !data.alwaysOn
        $('map').find('#' + id.substring(6)).data('maphilight', data).trigger('alwaysOn.maphilight');
        $('.booked-booth').find('#' + id).remove();
        $('.map').find('#' + id.substring(6)).mapster('deselect');
        
        if ($('.booked-booth').find('tr').length > 0) {
            $('#booking-btn').fadeIn();
        } else {
            $('#booking-btn').fadeOut();
        }

    });

    $('#booking-form').on('shown.bs.modal', function(e) {

        var orderedBooth = '';
        var totalBoothPrice = [];
        $('.booked-booth').find('tr').each(function() {
            var id = $(this).attr('id');
            var img = $(this).find('.booth-image').html();
            var unit = $(this).find('.booth-name').html();
            var price = $(this).find('.booth_price').attr('price');
            orderedBooth += '<tr id="booth-' + id + '">\n\
                                <td class="booth-image"> ' + img + '</td>\n\
                                <td class="booth_unit">' + unit + '<input type="hidden" name="booth_unit[]" class="booth_unit-form" value="' + unit + '" /></td>\n\
                                <td class="booth_price-detail" price="' + price + '">Rp. ' + price + '</td>\n\\n\
                                <input type="hidden" name="booth_price[]" class="booth_price-form" value="' + parseInt(price) + '" />\n\
                            </tr>\n\
                    ';

        });


        $('#purchased-item').html('');
        $('#purchased-item').html(orderedBooth);

        $('#purchased-item').find('.booth_price-detail').each(function() {
            totalBoothPrice.push(parseInt($(this).attr('price')));
        });
        var totalPrice = 0;

        for (var i = 0; i < totalBoothPrice.length; i++) {
            totalPrice += totalBoothPrice[i];
        }
        $('.total-harga-booth').attr('booth_price', totalPrice).html('Rp. ' + totalPrice);
        $('.total-harga-booth').digits();
        $('.booth_price-detail').digits();

        $('.single-item').slick({
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            infinite: false,
            adaptiveHeight: true
        });
    });

    $('#fullpage').fullpage({
        anchors: ['zonea', 'zoneb', 'zonec'],
        menu: '#main-nav',
        scrollingSpeed: 1000,
        css3: true,
    });



});

$.fn.digits = function() {
    return this.each(function() {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
}

function getBoothInfo(id, zone, baseurl) {

    var countRow = 0;

    $.ajax({
        url: baseurl + "/get-booth-data/"+id,
        type: "GET",
        dataType: 'json',
        async: false,
        success: function(msg){
            $.each(msg, function(i, data){
                var unit = data.name;
                var pict = '<img src="' + baseurl + '/' + data.image_link+'" />';
                var price = data.price;
                var table = '<tr id="booth-' + data.name + '">\n\
                         <td class="booth-image"> ' + pict + '</td>\n\
                         <td class="booth-name">' + unit + '</td>\n\
                         <td class="booth_price" price="' + price + '">Rp. ' + price + '</td>\n\
                         <td class="booth-action"><div class="btn btn-danger batal">Batal</div></td>\n\
                        </tr>\n\
                        ';
                $('.booked-booth').append(table);
                $('.booth_price').digits();
                $('#totalBooked').html($('.booked-booth').find('tr').length);
            })
        }
    })

    // $.getJSON(baseurl + "/get-booth-data/"+id, function(msg){
    //     $.each(msg, function(i, data){
    //         var unit = data.name;
    //         var pict = '<img src="' + baseurl + '/' + data.image_link+'" />';
    //         var price = data.price;
    //         var table = '<tr id="booth-' + data.id + '">\n\
    //                  <td class="booth-image"> ' + pict + '</td>\n\
    //                  <td class="booth-name">' + unit + '</td>\n\
    //                  <td class="booth_price" price="' + price + '">Rp. ' + price + '</td>\n\
    //                  <td class="booth-action"><div class="btn btn-danger batal">Batal</div></td>\n\
    //                 </tr>\n\
    //                 ';
    //         $('.booked-booth').append(table);
    //         $('.booth_price').digits();
    //         $('#totalBooked').html($('.booked-booth').find('tr').length);
    //     })
    // })

    // var pict = '<img src="' + baseurl + '/img/dummy-booth.jpg" />';
    // var unit = id;
    // var price = 20000000;
    // var tableRow = '';
    // var table = '<tr id="booth-' + id + '">\n\
    //                  <td class="booth-image"> ' + pict + '</td>\n\
    //                  <td class="booth-name">' + unit + '</td>\n\
    //                  <td class="booth_price" price="' + price + '">Rp. ' + price + '</td>\n\
    //                  <td class="booth-action"><div class="btn btn-danger batal">Batal</div></td>\n\
    //                 </tr>\n\
    //                 ';
    // $('.booked-booth').append(table);
    // $('.booth_price').digits();


}

function totalBoothCount(){

}

function removeBoothInfo(id, zone) {

    $('.booked-booth').find('#booth-' + id).remove();
     
    $('.booth_price').digits();


}

function getAllBoothStatus(baseurl) {
    $.getJSON(baseurl + "/get-all-booth", function(data, status) {
        $.each(data, function(key, val) {
            console.log(key);
            $('map').find('#' + key).attr({
                'status': 'booth-na',
                'consumer': val});
        });
        var dataArray = [];
        $('map').find('area').each(function() {

            if ($(this).attr('status') == 'booth-na') {
                dataArray.push({
                    key: $(this).attr('id'),
                    fillColor: "FF0000",
                    toolTip: $(this).attr('consumer'),
                    selected: true,
                    isSelectable: false,
                    strokeColor: "0000FF",
                });
                $(this).addClass("booth-booked");
            }
        });
        initMap(dataArray);
    });
}

function initMap(dataArray) {
    $('#zones').mapster({
        fillOpacity: 1,
        fillColor: "0000FF",
        stroke: true,
        strokeColor: "FF0000",
        strokeOpacity: 1,
        strokeWidth: 2,
        mapKey: 'name',
        showToolTip: true,
        //toolTipContainer: '<div></div>',
        areas: dataArray
    });
}
