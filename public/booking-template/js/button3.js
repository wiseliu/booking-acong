$(document).ready(function() {
	var tooltipArray = {1:'booth1',2:'booth2',3:'booth3',4:'booth4',5:'booth5'};
	var testArray = [
			{
                key: "1",
                fillColor: "000000",
				toolTip: tooltipArray[1],
				selected: true,
				isSelectable: false,
			},
			{
                key: "2",
                fillColor: "FF0000",
				toolTip: tooltipArray[2],
			},
		];
	$('#zones').mapster({
        fillOpacity: 1,
        fillColor: "ffff00",
        stroke: true,
        strokeColor: "3320FF",
        strokeOpacity: 1,
        strokeWidth: 4,
        mapKey: 'id',
        showToolTip: true,
		//toolTipContainer: '<div></div>',
        areas: testArray,
    });
});