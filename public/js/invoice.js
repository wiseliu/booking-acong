$(document).ready(function() {
    var pathArray = window.location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var baseurl = protocol + '//' + host;
    var length = pathArray.length;
    var id = pathArray[length-1];
    
    $('.print-btn').on('click', function(){
        window.location.replace('/print-invoice/'+id);
    })
})