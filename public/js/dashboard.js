$(function() {

    var pathArray = window.location.href.split('/');
    var protocol = pathArray[0];
    var host = pathArray[2];
    var baseurl = protocol + '//' + host;

    navigation();
    selectMedia();
    getAvilableTotalBooth(baseurl);
     getAllBooth(baseurl);
     getPaidBooth(baseurl);
    getBookedBooth(baseurl);
   
});

function getAvilableTotalBooth(baseurl) {

    $.getJSON(baseurl + "/get-available-booth", function(data) {
        var items = [];
        $.each(data, function(key, val) {
            items.push(val);
        });

        $('#available-booth').html(items.length);
        $('#available-booth').animateNumber({number: items.length}, 5000);

    });
}

function getPaidBooth(baseurl) {

    $.getJSON(baseurl + "/get-paid-booth", function(data) {
        var items = [];
        $.each(data, function(key, val) {
            items.push(val);
        });

        $('#paid-booth').html(items.length);
        $('#paid-booth').animateNumber({number: items.length}, 5000);

    });
}

function getAllBooth(baseurl) {
    
    $.getJSON(baseurl + "/get-booked-booth", function(data) {
        var table = '';
        $.each(data, function(key, val) {
            console.log(val);
            table += ' <tr> \n\
                            <td class="v-align-middle bold text-success">val</td>\n\
                            <td class="v-align-middle"> 3 Maret 2016</td>\n\
                            <td class="v-align-middle">Rp. 30,000,000</td>\n\
                            <td class="v-align-middle"><span class="label label-important">Unpaid</span></td>\n\
                            <td class="v-align-middle"><button type="button" data-toggle="modal" data-target="#viewConsumer" class="btn btn-small btn-primary">View</button</td>\n\
                        </tr>\n' ;
        });

       

    });
}

function getBookedBooth(baseurl) {

    var tot1 = '';
    var tot2 = [];

    $.ajax({
        dataType: "json",
        url: baseurl + "/get-all-booth",
        async: false, 
        success: function(data){
          var items1 = [];
            $.each(data, function(key, val) {
                items1.push(val);
            });
            tot1 = items1.length;  
        }
    });

    $.ajax({
        dataType: "json",
        url: baseurl + "/get-available-booth",
        async: false, 
        success: function(data){
          var items1 = [];
            $.each(data, function(key, val) {
                items1.push(val);
            });
            tot2 = items1.length;  
        }
    });

    $('#booked-booth').html(tot1- tot2);
    $('#booked-booth').animateNumber({number: tot1- tot2}, 5000);
}

function navigation() {
    var parts = window.location.href.split("/");
    var length = parts.length;
    var activeNav = parts[length - 1];

    $('#main-navigation').find('#' + activeNav).parent().parent().parent().addClass('open active');
    $('#main-navigation').find('#' + activeNav).parent().addClass('active');
}

function selectMedia() {

    $(document).on('click', '.thumb', function() {
        $(this).parent().toggleClass('active');
    });
}