/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$.validator.setDefaults({
    submitHandler: function() {
        alert("submitted!");
    }
});

$(function() {
    //test();
});

function validateUserManagementForm() {

    $("#register-form").validate({
        rules: {
            firstName: "required",
            password: {
                required: true,
                minlength: 6
            },
            confirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                number: true
            },
        },
        messages: {
            firstname: "Please enter your firstname",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
        }
    });

}

function test() {
    $(document).on('click', '#test', function() {
        $.ajaxSetup({
            header: $('meta[name="csrf-token"]').attr('content')
        })
        console.log($(this).closest('form').serialize());
        $.ajax({
            url: '/store-user',
            type: 'POST',
            dataType: 'json',
            data: $(this).closest('form').serialize(),
            success: function(data) {
                console.log(data);
            },
            error: function(data) {

                var errors = data.responseJSON;
                var msg =  errors.message;

                $.each(msg, function(index, val) {
                    $('#register-form').find('#' + index + "-Error").html('<strong>' + val + '</strong>');
                });


            }
        });
    });


}

