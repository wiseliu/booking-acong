$(document).ready(function() {
	var sum = 0;
	var units_left = 5;
	var units_selected = 0;
	var booth_selected = new Array();
	$('.map').maphilight({
		fillColor: 'FF0000',
		fillOpacity: 0.8,
	});
	$(document).on('click','.booth',function(e){
        var data = $(this).mouseout().data('maphilight') || {};
		var booth_id = $(this).attr('id');
        data.alwaysOn = !data.alwaysOn;
        $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
		$(this).toggleClass("booked");
		if($(this).hasClass("booth booked")){
			sum += 100;
			units_left -= 1;
			units_selected += 1;
			//console.log('booth1 is selected, sum now is: '+sum+' units left is: '+units_left+' units selected is: '+units_selected);
			booth_selected[booth_id] = 'booth'+booth_id+': 100 </br>';
			$('#selected-unit-span-container').html(booth_selected);
		}
		else{
			sum -= 100;
			units_left += 1;
			units_selected -= 1;
			//console.log('booth1 is unselected, sum now is: '+sum+' units left is: '+units_left+' units selected is: '+units_selected);
			booth_selected[booth_id] = '';
			$('#selected-unit-span-container').html(booth_selected);
		}
		$('#price-sum-container').html(sum);
		$('#units-left-container').html(units_left);
		$('#units-selected-container').html(units_selected);
		
    });
	
	$('#book-button').click(function(e) {
		var sum = 0;
		var units_left = 5;
		var units_selected = 0;
		var booth_selected = [];
		alert('your unit has been booked');
		location.reload();
	})
});