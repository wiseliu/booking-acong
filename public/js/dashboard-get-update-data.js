/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {

    var id = '';
    var fname = '';
    var lname = '';
    var email = '';

    $(document).on('click', '.update-item', function() {
        id = $(this).attr('id');

        $.ajaxSetup({
            header: $('meta[name="csrf-token"]').attr('content')
        })
        $.ajax({
            url: './get-user-data/' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                $('#edit_user_fname').val(data.user_fname);
                $('#edit_user_lname').val(data.user_lname);
                $('#edit_user_email').val(data.user_email);
                $('#edit_role_id').val(data.role_id);
                $('#edit_user_id').val(data.user_id);
            },
            error: function(data) {

                var errors = data.responseJSON;
                var msg = errors.message;

                $.each(msg, function(index, val) {
                    $('#register-form').find('#' + index + "-Error").html('<strong>' + val + '</strong>');
                });
            }
        });
    });


    $(document).on('click', '.delete-item', function() {
        id = $(this).attr('id');
        $('#delete-user-id').val(id);

    });

});


